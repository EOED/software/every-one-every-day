package readers.nfc.com.nfc_reader2;

import java.util.ArrayList;

public class DataHolder{

    final ArrayList<Contact> users = new ArrayList<>();
    private  DataHolder(){}

    static DataHolder getInstance()
    {
        if(instance == null){
            instance = new DataHolder();
        }
        return  instance;
    }

    public void addUser(String vid, String idtag){
        users.add(new Contact(vid, idtag));
    }


    public  int getCount()
    {
        return users.size();
    }

    private  static DataHolder instance;
}
