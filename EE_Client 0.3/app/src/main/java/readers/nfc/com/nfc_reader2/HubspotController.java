package readers.nfc.com.nfc_reader2;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

public class HubspotController {

public void startThread(final String[] details)
{
  //  String[] details;


    Thread t = new Thread(){
      public  void run()
      {
          String text = "";
          BufferedReader reader=null;

          try {

              String data =  URLEncoder.encode("First Name", "UTF-8")
                      + "=" + URLEncoder.encode(details[0], "UTF-8");
              data += "&" + URLEncoder.encode("Last Name", "UTF-8") + "="
                      + URLEncoder.encode(details[1], "UTF-8");
              data += "&" + URLEncoder.encode("Email", "UTF-8") + "="
                      + URLEncoder.encode(details[2], "UTF-8");
              data += "&" + URLEncoder.encode("Phone", "UTF-8") + "="
                      + URLEncoder.encode(details[3], "UTF-8");
              data += "&" + URLEncoder.encode("zip", "UTF-8") + "="
                      + URLEncoder.encode(details[4], "UTF-8");

              // Defined URL  where to send data
              URL url = new URL("https://forms.hubspot.com/uploads/form/v2/5534968/");

              // Send POST data request

              URLConnection conn = url.openConnection();
              conn.setDoOutput(true);
              OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
              wr.write( data );
              wr.flush();
              System.out.println("Submitted.");
              // Get the server response

              reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
              StringBuilder sb = new StringBuilder();
              String line = null;

              // Read Server Response
              while((line = reader.readLine()) != null)
              {
                  // Append server response in string
                  sb.append(line + "\n");
              }


              text = sb.toString();
          }
          catch(Exception ex)
          {
              System.out.println(ex.toString());
          }
          finally
          {
              try
              {

                  reader.close();
              }

              catch(Exception ex) {}
          }
      }


    };
    t.start();

}



public void postData(final String url)
{

    //startThread(details);

   // sendDatabasePost(url, "");

}




}

