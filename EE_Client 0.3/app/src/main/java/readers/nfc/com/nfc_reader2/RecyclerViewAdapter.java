package readers.nfc.com.nfc_reader2;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.content.SharedPreferences;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>{

    Utils utils;
    private  static  final String tag = "RecyclerViewAdapter";
    private ArrayList<String> mImageNames = new ArrayList<>();
    private  ArrayList<String> mImages = new ArrayList<>();
    private Context mContext;
String vid = "";
    public RecyclerViewAdapter( Context mContext, ArrayList<String> imageNames, ArrayList<String> images,String vid) {
        this.mContext = mContext;
        this.mImageNames = imageNames;
        this.mImages = images;
        this.vid = vid;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view  = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_listitem, viewGroup, false);
   ViewHolder viewHolder = new ViewHolder(view);
   return  viewHolder;

    }
    public static void setDefaults(String key, String value, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getDefaults(String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key, null);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

   final String shopLoc = getDefaults("location", mContext);
   //  String shopLocation =
        Glide.with(mContext).asBitmap().load(mImages.get(i)).into(viewHolder.image);
    final String imageName = mImageNames.get(i);
        viewHolder.imageName.setText(imageName)    ;

        viewHolder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
 utils = new Utils();
                Long tsLong = System.currentTimeMillis();
              if(imageName.contains("I'm signing out"))
              {
                  Long tfLong = tsLong + 30000;
                  utils.addMeeting(imageName, vid, tsLong, tfLong, shopLoc);
                  Intent intent = new Intent(mContext, SurveyActivity.class);
                  mContext.startActivity(intent);
              }
              else
              {
                  Long tfLong = tsLong + 3600000;
                  utils.addMeeting(imageName, vid, tsLong, tfLong, shopLoc);
                  Intent intent = new Intent(mContext, MainActivity.class);
                  mContext.startActivity(intent);
                  Toast.makeText(mContext, "Signed in.", Toast.LENGTH_SHORT).show();
              }
            }
        });
    }


    @Override
    public int getItemCount() {
        return  mImageNames.size();
           }

    public  class ViewHolder extends RecyclerView.ViewHolder{

ImageView image;
TextView imageName;
RelativeLayout parentLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            imageName =     itemView.findViewById(R.id.imageName);
            parentLayout = itemView.findViewById(R.id.parent_layout);
        }
    }

}
